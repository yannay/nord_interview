import axios from 'axios';

class Ptsv2 {
  async get() {
    try {
      const response = await axios.get('http://ptsv2.com/t/fu807-1554722621/post');
      //console.log(response.data);
      return response;
    } catch (error) {
      //console.error(error);
      return error.response;
    }
  }

  async post(url: string, username: string, password: string) {
    try {
      const response = await axios.post(
        url,
        {},
        {
          auth: {
            username: username,
            password: password
          }
        }
      );
      //console.log(response.data);
      //console.log(response.request);
      return response;
    } catch (error) {
      //console.error(error);
      return error.response;
    }
  }
}

export default new Ptsv2();
