class Codes {
  check_200(status: number) {
    expect(status).toBe(200, 'Status code should be 200');
  }

  check_401(status: number) {
    expect(status).toBe(401, 'Status code should be 401');
  }
}

export default new Codes();
