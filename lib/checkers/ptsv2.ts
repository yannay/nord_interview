class Ptsv2Check {
  check_get_json(data: any) {
    expect(data).toEqual(
      {
        username: 'automate',
        password: 'everything',
        targetUrl: 'http://ptsv2.com/t/7ty82-1554722743/post'
      },
      'Incorrect json get request'
    );
  }

  check_post_json(data: any) {
    expect(data).toEqual(
      {
        ip: '192.168.2.2',
        token: '0799249366'
      },
      'Incorrect json post request'
    );
  }
}

export default new Ptsv2Check();
