#FROM selenium/node-chrome:98.0
FROM node:16

WORKDIR /node

RUN node --version

RUN npm --version

RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

RUN apt-get update && apt-get -f install -y ./google-chrome-stable_current_amd64.deb

COPY . .

RUN npm ci

#RUN npm run test-api

#RUN npm test

# ENTRYPOINT ["sleep"]
# CMD ["99999"]
