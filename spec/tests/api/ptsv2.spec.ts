// import Player from '../../lib/jasmine_examples/Player';
// import Song from '../../lib/jasmine_examples/Song';
import Ptsv2 from '../../../lib/services/ptsv2';
import Codes from '../../../lib/checkers/codes';
import Ptsv2Check from '../../../lib/checkers/ptsv2';

describe('Ptsv2 Get', function () {
  it('Get Request', async function () {
    const res = await Ptsv2.get();
    Codes.check_200(res.status);
    Ptsv2Check.check_get_json(res.data);
  });
});

describe('Ptsv2 Post', function () {
  let res: any;

  beforeAll(async function () {
    res = await Ptsv2.get();
    //res = await Ptsv2.post(res.data.targetUrl,res.data.username,res.data.password)
  });

  it('Post Positive', async function () {
    //let res = await Ptsv2.get()
    let response = await Ptsv2.post(res.data.targetUrl, res.data.username, res.data.password);
    Codes.check_200(response.status);
  });

  it('Post Negative', async function () {
    let response = await Ptsv2.post(res.data.targetUrl, res.data.username, '');
    Codes.check_401(response.status);
  });

  it('Post Check Json', async function () {
    let response = await Ptsv2.post(res.data.targetUrl, res.data.username, res.data.password);
    Codes.check_200(response.status);
    Ptsv2Check.check_post_json(response.data);
  });
});
