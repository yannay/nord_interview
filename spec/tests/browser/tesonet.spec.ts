import { Browser, by, have, their } from 'selenidejs';
import { Builder, Capabilities } from 'selenium-webdriver';
import { Options } from 'selenium-webdriver/chrome';
export let browser: Browser;

describe('Tesonet', function () {
  beforeAll(async () => {

    const opts = new Options().addArguments('--headless', 'window-size=1192,870', '--no-sandbox', '--disable-dev-shm-usage');

    const driver = new Builder()
      .forBrowser('chrome')
      .setChromeOptions(opts)
      .withCapabilities(Capabilities.chrome())
      .build();

    browser = Browser.configuredWith().driver(driver).timeout(4000).build();

    jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;
  });

  it('Count the number of categories', async function () {
    await browser.open('https://www.tesonet.com');

    await browser.should(have.title('We are Tesonet. We build the digital future.'));

    await browser.element(by.exactText('Career')).click();

    await browser.should(have.title('Career | We are Tesonet'));

    const categories_amount = await browser.all(by.className('career-lever-v3__job-category-content')).get(their.size);

    console.log(`Number of available categories: ${categories_amount}`);

    // const categories = browser.all('.career-lever-v3__job-category-content>p');

    // for (const cat of categories) {

    // }

    //console.log(categories)
  });

  afterAll(async () => {
    await browser.quit();
  });
});
